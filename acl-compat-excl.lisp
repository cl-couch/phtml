
(in-package :phtml)

(defvar *current-case-mode*
  #+allegro excl::*current-case-mode*
  #-allegro :case-insensitive-upper
)

(defun intern* (s len package)
  (intern (subseq s 0 len) package))