
(in-package :phtml)

(defmacro without-scheduling (&body forms)
  #+allegro `(mp::without-scheduling ,@forms)
  #-allegro `(progn ,@forms)
)