;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; -*-

(defpackage :phtml.system (:use :asdf :cl))
(in-package :phtml.system)

(defsystem phtml
  :name "phtml"
  :components
  ((:file "packages")
   (:file "ifstar" :depends-on ("packages"))
   (:file "acl-compat-mp" :depends-on ("packages"))
   (:file "acl-compat-excl" :depends-on ("packages"))
   (:file "phtml" :depends-on ("packages" "ifstar" "acl-compat-mp" "acl-compat-excl"))))

